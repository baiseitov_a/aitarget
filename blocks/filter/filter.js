$(function () {

  var $filterNode = $('.js-filter');
  var $filterTypeSelect = $filterNode.find('.js-filter__type');
  var selectedTagField = $('.filter-selected-field');
  var $filterTagsCatalog = $('.filter-tags-catalog');
  var $filterSearchCatalog = $('.filter-search-catalog');

  $(document).on('click', '.js-filter__type', function (e) {
    var filterType = $(this).attr('data-filter-set');
    if ( $filterNode.attr('data-filter-by') == filterType ) {
      $filterSearchCatalog.slideUp(250);
      $filterTagsCatalog.slideUp(250);
      $filterNode.attr('data-filter-by', '');
    }
    else {
      $filterNode.attr('data-filter-by', filterType);
      if (filterType == 'tags') {
        $filterTagsCatalog.slideDown()
        $filterSearchCatalog.slideUp()
      }
      if (filterType == 'search') {
        $filterSearchCatalog.slideDown()
        $filterTagsCatalog.slideUp()
      }
    }
  })

  $(document).on('click', function (e) {    
    if ($(e.target).parents('.js-filter').length && !$(e.target).is('.js-filter__close') || $(e.target).is('.filter-selected-field__delete')) return
    $('.filter-search-catalog, .filter-tags-catalog').slideUp(250)
    setTimeout(function() {
      $filterNode.attr('data-filter-by', '');  
    }, 200)
  })

  $(document).on('click', '.filter-tags-catalog .filter-tags-catalog__item', function (e) {
    e.preventDefault();
    var tag = $(this).find('[data-tag]');
    var tagValue = tag.attr('data-tag');
    if ( tag.hasClass('is-selected') ){
      tag.removeClass('is-selected');
      selectedTagField.find('[data-tag="' + tagValue + '"]').remove()
    } else {
      tag.addClass('is-selected');
      selectedTagField.append('<span class="filter-selected-field__tag" data-tag="' + tagValue + '">#' + tagValue + '<span class="filter-selected-field__delete icon-icon-cross"></span></span>');
    }
  })

  $(document).on('click', '.filter-selected-field__delete', function (e) {
    e.preventDefault();
    var tag = $(this).parents('[data-tag]');
    var tagValue = tag.attr('data-tag');
    $filterTagsCatalog.find('[data-tag="' + tagValue + '"]').removeClass('is-selected');
    tag.addClass('is-delete');
    setTimeout(function() {
      tag.remove();
    }, 350);
  })

})