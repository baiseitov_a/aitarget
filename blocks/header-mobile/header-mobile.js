$(function () {

  $(document).on('click', '.header__toggle', function () {
    $('.header').removeClass('is-search-dropdown-open')
                .toggleClass('is-nav-dropdown-open');
    $('.header-nav-dropdown').slideToggle();
    $('.header-search-dropdown').slideUp();
  })

  $(document).on('click', '.header__search', function () {
    $('.header').removeClass('is-nav-dropdown-open')
                .toggleClass('is-search-dropdown-open');
    $('.header-search-dropdown').slideToggle();
    $('.header-nav-dropdown').slideUp();
  })

})