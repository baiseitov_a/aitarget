$(function () {

  $('.header-lang').click(function () {
    $(this).toggleClass('is-opened')
  })

  $('.header-lang .select2').on('select2:close', function (e) {
    $('.header-lang').removeClass('is-opened')
  });

})