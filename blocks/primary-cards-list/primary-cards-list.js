$(function () {

  $(document).on('click', '.primary-cards-list__button', function(e){
    e.preventDefault();
    const url = $(this).attr('href');
    $.ajax({
      type: "get",
      url: url,
      success: function (data) {
        if (data['error']) {
          console.log('error');
        }
        $('.primary-cards-list').append(data);
      },
      error: function (xhr, ajaxOptions, thrownError) {
        console.log(xhr, thrownError, ajaxOptions);
      }
    });

  })

})