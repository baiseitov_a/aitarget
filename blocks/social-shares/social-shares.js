$(function () {

  $(document).on('click', '.social-shares__toggle', function () {
    var parentBlock = $(this).parents('.social-shares');
    parentBlock.find('.social-shares__list').slideToggle()
    if ( parentBlock.hasClass('is-open') ){
      setTimeout(() => {
        parentBlock.removeClass('is-open')
      }, 400);
    }  
    else{
      parentBlock.addClass('is-open')
    }
  })

})