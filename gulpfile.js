
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var debug = require('gulp-debug');
var	watcher = require('gulp-watch');
var	runSequence = require('run-sequence');
var browserSync = require('browser-sync').create();

var config = {
	paths: {
		base: './',
		js: './js/',
		css: './css/',
		sass: './sass/',
		blocks: './blocks/',
		helpers: './sass/helpers/',
		tmp: './tmp/'
	},

	browsers: [
		'ie >= 11',
		'ff >= 29',
		'Opera >= 12',
		'iOS >= 6',
		'Chrome >= 28',
		'Android >= 2'
	],
};

var paths = config.paths;
var browsers = config.browsers;

gulp.task('sass', function () {
	gulp.src(['*.scss', '!_*.scss', 'helpers/*.scss', '!helpers/_*.scss'], {cwd: paths.sass})
		.pipe(sass({
			outputStyle: 'expanded'
		})
		.on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: browsers,
			cascade: true,
			remove: false
		}))
		.pipe(gulp.dest(paths.css))
		.pipe(browserSync.stream());
});

gulp.task('concat:scss', function() {
	return gulp.src([paths.blocks + '**/*.scss'])
		.pipe(concat('_blocks.scss'))
		.pipe(gulp.dest(paths.tmp));
});

gulp.task('concat:blockJs', function() {
	var targets = [paths.blocks + '**/*.js'];

	return gulp.src(targets)
		.pipe(concat('blocks.js'))
		.pipe(gulp.dest(paths.js))
		.pipe(browserSync.stream());
});

gulp.task('concat', ['concat:blockJs', 'concat:scss']);

gulp.task('watch', function() {
	watcher([paths.blocks + '**/*.scss'], function () {
		gulp.start('concat:scss');
	});

	watcher(paths.blocks + '**/*.js', function () {
		gulp.start('concat:blockJs');
	});

	watcher([paths.sass + '**/*.scss', paths.tmp + '**/*.scss'], function () {
		gulp.start('sass');
	});

	watcher([paths.base + '*.html'], function () {
		browserSync.reload();
	});
});

gulp.task('sync', function() {
    browserSync.init({
        server: true
    });
});

gulp.task('default', function() {
	return runSequence(
		'concat',
		'sass',
		'watch',
		'sync'
	);
});
