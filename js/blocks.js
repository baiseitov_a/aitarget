$(function () {

  if( $('.js-flip').length ){
    $(".js-flip").flip();
  }
  
})
$(function () {

  var $filterNode = $('.js-filter');
  var $filterTypeSelect = $filterNode.find('.js-filter__type');
  var selectedTagField = $('.filter-selected-field');
  var $filterTagsCatalog = $('.filter-tags-catalog');
  var $filterSearchCatalog = $('.filter-search-catalog');

  $(document).on('click', '.js-filter__type', function (e) {
    var filterType = $(this).attr('data-filter-set');
    if ( $filterNode.attr('data-filter-by') == filterType ) {
      $filterSearchCatalog.slideUp(250);
      $filterTagsCatalog.slideUp(250);
      $filterNode.attr('data-filter-by', '');
    }
    else {
      $filterNode.attr('data-filter-by', filterType);
      if (filterType == 'tags') {
        $filterTagsCatalog.slideDown()
        $filterSearchCatalog.slideUp()
      }
      if (filterType == 'search') {
        $filterSearchCatalog.slideDown()
        $filterTagsCatalog.slideUp()
      }
    }
  })

  $(document).on('click', function (e) {    
    if ($(e.target).parents('.js-filter').length && !$(e.target).is('.js-filter__close') || $(e.target).is('.filter-selected-field__delete')) return
    $('.filter-search-catalog, .filter-tags-catalog').slideUp(250)
    setTimeout(function() {
      $filterNode.attr('data-filter-by', '');  
    }, 200)
  })

  $(document).on('click', '.filter-tags-catalog .filter-tags-catalog__item', function (e) {
    e.preventDefault();
    var tag = $(this).find('[data-tag]');
    var tagValue = tag.attr('data-tag');
    if ( tag.hasClass('is-selected') ){
      tag.removeClass('is-selected');
      selectedTagField.find('[data-tag="' + tagValue + '"]').remove()
    } else {
      tag.addClass('is-selected');
      selectedTagField.append('<span class="filter-selected-field__tag" data-tag="' + tagValue + '">#' + tagValue + '<span class="filter-selected-field__delete icon-icon-cross"></span></span>');
    }
  })

  $(document).on('click', '.filter-selected-field__delete', function (e) {
    e.preventDefault();
    var tag = $(this).parents('[data-tag]');
    var tagValue = tag.attr('data-tag');
    $filterTagsCatalog.find('[data-tag="' + tagValue + '"]').removeClass('is-selected');
    tag.addClass('is-delete');
    setTimeout(function() {
      tag.remove();
    }, 350);
  })

})
//Прокрутка для кнопки наверх
$(document).ready(function(){
  $('.footer__arrow-up').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 700);
    return false;
  });
});
// Обработка стандартной формы
	$(function () {
		$('.form-standart').livequery(function () {
			var $context = $(this);
			var $form = $('form', $context);

			$form.initWebForm('form-standart');

			$('[data-mask="phone"]', $context).each(function(){
				$(this).mask('+7 (999) 999-99-99');
			});

			// Появление полей для смены пароля
			$('.js-pass-link', $context).on('click', function () {
				$context.find('[name=changepassword]').val('1');
				$('.js-pass-change', $context).addClass('is-visible');

				$(this).closest('.form-standart__field').addClass('is-hidden');
				return false;
			});

			$('.js-pass-cancel', $context).on('click', function () {
				$context.find('[name=changepassword]').val('');
				$('.js-pass-change', $context).removeClass('is-visible');
				$('.js-pass-link', $context).closest('.form-standart__field').removeClass('is-hidden');
				return false;
			});
		});
	});

$(function () {

  $('.header-lang').click(function () {
    $(this).toggleClass('is-opened')
  })

  $('.header-lang .select2').on('select2:close', function (e) {
    $('.header-lang').removeClass('is-opened')
  });

})
$(function () {

  $(document).on('click', '.header__toggle', function () {
    $('.header').removeClass('is-search-dropdown-open')
                .toggleClass('is-nav-dropdown-open');
    $('.header-nav-dropdown').slideToggle();
    $('.header-search-dropdown').slideUp();
  })

  $(document).on('click', '.header__search', function () {
    $('.header').removeClass('is-nav-dropdown-open')
                .toggleClass('is-search-dropdown-open');
    $('.header-search-dropdown').slideToggle();
    $('.header-nav-dropdown').slideUp();
  })

})
$(function () {

  $(document).on('click', '.primary-cards-list__button', function(e){
    e.preventDefault();
    const url = $(this).attr('href');
    $.ajax({
      type: "get",
      url: url,
      success: function (data) {
        if (data['error']) {
          console.log('error');
        }
        $('.primary-cards-list').append(data);
      },
      error: function (xhr, ajaxOptions, thrownError) {
        console.log(xhr, thrownError, ajaxOptions);
      }
    });

  })

})
$(function () {

  $(document).on('click', '.social-shares__toggle', function () {
    var parentBlock = $(this).parents('.social-shares');
    parentBlock.find('.social-shares__list').slideToggle()
    if ( parentBlock.hasClass('is-open') ){
      setTimeout(() => {
        parentBlock.removeClass('is-open')
      }, 400);
    }  
    else{
      parentBlock.addClass('is-open')
    }
  })

})
// comments
	$(function () {
		$('.block-name').livequery(function () {
			var $context = $(this);

			// code here...
		})
	});
